
class RecordProcessor
	def initialize(record_container)
		@record_container = record_container
	end

	def update_record_container
		@record_container.get_record_array.each do |product|
			product.process
		end
	end
end