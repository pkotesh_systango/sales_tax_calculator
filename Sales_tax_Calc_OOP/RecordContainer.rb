require 'singleton'

class RecordContainer
	include Singleton
	
	def record_array
		@record_array ||= []
	end
		
	def add(product)
		record_array << product
	end

	def get_record_array
		record_array
	end

end