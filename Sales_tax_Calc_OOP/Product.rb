require_relative 'TaxStrategyFactory.rb'

class Product
	def initialize(id, name, price, country)
		@id, @name, @price, @country = id, name, price, country
		@sales_tax,@final_price = 0,0
	end
	
	def process
		#set_sales_tax
		tax_strategy = ::TaxStrategyFactory.new
		tax_strategy_result = tax_strategy.get_strategy(self)
		@sales_tax = (tax_strategy_result == 0) ? 0 : (tax_strategy_result.sales_tax)
		set_final_price
	end

	def set_final_price
		@final_price = @price + @sales_tax
	end

	def get_id
		@id
	end
	def get_name
		@name
	end
	def get_price
		@price
	end
	def get_country
		@country
	end
	def get_sales_tax
		@sales_tax
	end
	def get_final_price
		@final_price
	end
	
end