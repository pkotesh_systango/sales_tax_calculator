require 'csv'
require_relative 'SalesTaxCalculator'
INPUT_FILENAME = 'input.csv'
OUTPUT_FILENAME = 'output.csv'

sales_tax_calculator = SalesTaxCalculator.new(INPUT_FILENAME, OUTPUT_FILENAME)
sales_tax_calculator.run
	