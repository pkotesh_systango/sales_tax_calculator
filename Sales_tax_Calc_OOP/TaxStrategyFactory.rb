require_relative 'CountryModule'
require_relative 'IndiaStrategy.rb'
require_relative 'JapanStrategy.rb'
require_relative 'EgyptStrategy.rb'

class TaxStrategyFactory
	include Country
	def get_strategy(product)
		country = product.get_country.to_s.downcase 
		return IndiaStrategy.new(product) if Country::IND == country
		return EgyptStrategy.new(product) if Country::EGY == country
		return JapanStrategy.new(product) if Country::JAP == country
		puts "We do not support this country::#{country}"
		return 0
	end
end