require 'csv'
require_relative 'FileValidator.rb'

class InputReader
	def initialize(input_filename)
		@input_filename = input_filename
		validate_file
  end

	def validate_file
		puts "File doesn't exist" if(!FileValidator.check_file(@input_filename))
		puts "File doesn't have read access" if(!FileValidator.check_access(@input_filename))
	end

	def read_file
		return CSV.read(@input_filename, converters: :numeric)
	end
end