require 'csv'
require_relative 'FileValidator.rb'

class OutputWriter
	def initialize(record_container, output_filename)
		@record_container  = record_container
		@output_filename = output_filename
	end

	def write_to_output_file
		CSV.open(@output_filename,'w') #Create Output file
		validate_file
		CSV.open(@output_filename,'a+') do |csv_object|
			@record_container.get_record_array.each do |product|
				csv_object << [product.get_id,product.get_name,product.get_price,product.get_country,product.get_sales_tax,product.get_final_price]
			end
		end
	end

	def validate_file
		puts "File doesn't exist output" if(!FileValidator.check_file(@output_filename))
		puts "File doesn't have read access" if(!FileValidator.check_access(@output_filename))
	end
end