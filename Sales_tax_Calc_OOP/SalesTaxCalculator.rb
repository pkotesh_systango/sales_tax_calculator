require_relative 'InputReader.rb'
require_relative 'RecordContainer.rb'
require_relative 'RecordInitializer.rb'
require_relative 'RecordProcessor.rb'
require_relative 'OutputWriter.rb'

class SalesTaxCalculator
	def initialize(input_file, output_file)
		@input_file, @output_file = input_file, output_file
	end

	def run
		input_reader_instance = InputReader.new(@input_file)
		record_container_instance = RecordContainer.instance
		record_initializer_instance = RecordInitializer.new(input_reader_instance,record_container_instance) 
		record_processor_instance = RecordProcessor.new(record_container_instance)
		output_writer_instance = OutputWriter.new(record_container_instance,@output_file)

		record_initializer_instance.populate_product
		record_processor_instance.update_record_container
		output_writer_instance.write_to_output_file
		
	end
end