require_relative 'Product.rb'
require_relative 'CountryStrategy.rb'

class IndiaStrategy < CountryStrategy
	def initialize (product)
		@product = product
	end

	def sales_tax
		return 0 if @product.get_price < 100
		return @product.get_price * 0.1 if @product.get_price >= 100 && @product.get_price < 500
		return @product.get_price * 0.15 if @product.get_price < 1000
		return @product.get_price * 0.2
	end
end