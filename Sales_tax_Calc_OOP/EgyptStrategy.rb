require_relative 'Product.rb'
require_relative 'CountryStrategy.rb'

class EgyptStrategy < CountryStrategy
	def initialize(product)
		@product = product
	end

	def sales_tax
		(@product.get_price > 1000) ? (@product.get_price * 0.3) : 0
	end
end