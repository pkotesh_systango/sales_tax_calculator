require_relative 'Product.rb'
require_relative 'CountryStrategy.rb'

class JapanStrategy < CountryStrategy
	def initialize(product)
		@product = product
	end

	def sales_tax
		(@product.get_price > 10000) ? (@product.get_price * 0.02) : 0
	end
end