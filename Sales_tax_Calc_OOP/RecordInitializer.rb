require_relative 'Product.rb'
class RecordInitializer
	def initialize(input_reader,record_container)
		@input_reader, @record_container = input_reader, record_container
	end	

	def populate_product()
		input_array = @input_reader.read_file
		input_array.each do |row|
			product_from_row = Product.new(row_id(row),row_name(row),row_price(row),row_country(row))
			@record_container.add(product_from_row)
		end
	end

	def row_id(row)
		row[0].to_i rescue 0
	end
	def row_name(row)
		row[1].to_s rescue ''
	end
	def row_price(row)
		row[2].to_i rescue 0
	end
	def row_country(row)
		row[3].to_s rescue ''
	end

end