require 'csv'

INPUT_FILENAME = 'input.csv'
OUTPUT_FILENAME = 'output.csv'
IND = "india"
EGY = "egypt"
JAP = "japan"

class Product
	def initialize(id, name, price, country)
		@id, @name, @price, @country = id, name, price, country
	end
	@sales_tax,@final_price = 0,0
	
	def set_sales_tax
		@sales_tax = calculate_sales_tax
	end

	def set_final_price
		@final_price = @price + @sales_tax
	end

	def calculate_sales_tax
		@country = @country.to_s.downcase 
		return sales_tax_india if IND == @country
		return sales_tax_egypt if EGY == @country
		return sales_tax_japan if JAP == @country
		puts "We do not support this country::#{@country}"
		return  0
	end

	def sales_tax_india
		return 0 if @price < 100
		return @price * 0.1 if @price >= 100 && @price < 500
		return @price * 0.15 if @price < 1000
		return @price * 0.2
	end

	def sales_tax_egypt
		(@price > 1000) ? (@price * 0.3) : 0
	end

	def sales_tax_japan
		(@price > 10000) ? (@price * 0.02) : 0
	end

	def get_id
		@id
	end
	def get_name
		@name
	end
	def get_price
		@price
	end
	def get_country
		@country
	end
	def get_sales_tax
		@sales_tax
	end
	def get_final_price
		@final_price
	end
	
end

class InputReader
	def initialize(filename)
		@filename = filename
	end

	def validate_file
		(File.file?(@filename))? "Input file present" : "Input file doesn't exist"
		puts "Opened Input File"
	end

	def read_file
		return CSV.read(@filename, converters: :numeric)
	end
end

class RecordInitializer
	def initialize(input_reader,record_container)
		@input_reader, @record_container = input_reader, record_container
	end	

	def assign_row_to_product()
		input_array = @input_reader.read_file
		input_array.each do |row|
			product_from_row = Product.new(row_id(row),row_name(row),row_price(row),row_country(row))
			@record_container.add(product_from_row)
		end
	end

	def row_id(row)
		row[0].to_i rescue 0
	end
	def row_name(row)
		row[1].to_s rescue ''
	end
	def row_price(row)
		row[2].to_i rescue 0
	end
	def row_country(row)
		row[3].to_s rescue ''
	end

end

class RecordContainer
	def record_array
		@record_array ||= []
	end
		
	def add(product)
		record_array << product
	end

	def get_record_array
		record_array
	end

end

class RecordProcessor
	def initialize(record_container)
		@record_container = record_container
	end

	def update_record_container
		@record_container.get_record_array.each do |product|
			product.set_sales_tax
			product.set_final_price
		end
	end
end

class OutputWriter
	def initialize(record_container, output_filename)
		@record_container, @output_filename = record_container, output_filename
	end

	def write_to_output_file
		CSV.open(@output_filename,'w') do |csv_object|
			@record_container.get_record_array.each do |product|
				csv_object << [product.get_id,product.get_name,product.get_price,product.get_country,product.get_sales_tax,product.get_final_price]
			end
		end
	end

	def validate_output_file
		return "Error! Output file is not created" if !(File.file?(@output_filename))
		puts "Created Output File"
	end
end

class SalesTaxCalculator
	def initialize(input_file, output_file)
		@input_file, @output_file = input_file, output_file
	end

	def run
		input_reader_instance = InputReader.new(@input_file)
		record_container_instance = RecordContainer.new
		record_initializer_instance = RecordInitializer.new(input_reader_instance,record_container_instance) 
		record_processor_instance = RecordProcessor.new(record_container_instance)
		output_writer_instance = OutputWriter.new(record_container_instance,@output_file)

		input_reader_instance.validate_file
		record_initializer_instance.assign_row_to_product
		record_processor_instance.update_record_container
		output_writer_instance.write_to_output_file
		output_writer_instance.validate_output_file
	end
end

sales_tax_calculator = SalesTaxCalculator.new(INPUT_FILENAME, OUTPUT_FILENAME)
sales_tax_calculator.run
