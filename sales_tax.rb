require 'csv'

INPUT_FILENAME = "input.csv"
OUTPUT_FILENAME = 'output.csv'
IND = "india"
EGY = "egypt"
JAP = "japan"

read_and_process_csv
create_output_csv

def read_and_process_csv
  CSV.foreach(INPUT_FILENAME, converters: :numeric) do |row|
    process_row_for_sales_tax(row)
  end 
end

def create_output_csv
  output_array = CSV.read(INPUT_FILENAME)

  output_array.each do |output_row|
    output_row << calculated_sales_tax.shift
    output_row << calculated_final_price.shift
  end

  #create output.csv file
  CSV.open(OUTPUT_FILENAME, 'w') do |csv_object|
    output_array.each do |row_array|
      csv_object << row_array
    end
  end

end


def process_row_for_sales_tax(row)
  sales_tax = calculate_sales_tax(row)
	calculated_sales_tax << sales_tax
	calculated_final_price << calculate_final_price(price_from_row(row), sales_tax)
end


def calculate_sales_tax(row)
	country = country_from_row(row)
	return sales_tax_india(price_from_row(row)) if IND == country
	return sales_tax_egypt(price_from_row(row)) if EGY == country
	return sales_tax_japan(price_from_row(row)) if JAP == country
	puts "We do not support this country::#{country}"
	return  0
end

def country_from_row(row)
  row[3].to_s.downcase rescue ''
end

def price_from_row(row)
	row[2].to_i rescue 0
end

def sales_tax_india(price)
	return 0 if price < 100
	return price * 0.1 if price >= 100 && price < 500
	return price * 0.15 if price < 1000
	return price * 0.2
end

def sales_tax_egypt(price)
	(price > 1000) ? (price * 0.3) : 0
end

def sales_tax_japan(price)
	(price > 10000) ? (price * 0.02) : 0
end

def calculate_final_price(price,sales_tax)
  price + sales_tax
end

def calculated_sales_tax
	@calculated_sales_tax ||= []
end
 
def calculated_final_price 
	@calculated_final_price ||= []
end